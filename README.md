# La Racine

Plateforme web pour la gestion des Paniers Bio.

# Installer La Racine pour le développement  

( 0. Installer Git si vous ne l'avez pas )

1. Cloner le repo dans votre dossier de travail La Racine:  
```
git clone https://gitlab.com/TristanDLM/la-racine.git
```

2. Si vous n'avez pas NodeJS, télécharger le depuis https://nodejs.org/ (LTS ou current) et l'installer.  
Vérifier que l'installation a fonctionné:
```
node --version
```  
Si vous voyez la version, c'est bon.

3. Dans la racine du projet, vérifier la présence du **package.json** et télécharger les dépendances npm pour le serveur avec:  
```
npm i
```

4. Naviguer dans le répertoire **/client/** et télécharger les dépendances du client avec:
```
cd client/
npm i
```

### Lancer le serveur (backend)
Dans la racine:
```
npm run dev
```
Le serveur backend devrait être démarré sur ***localhost:5000*** .

### Lancer le client (frontend)
Dans **/client/**:
```
npm run serve
```
Le serveur client devrait être démarré sur ***localhost:8080*** .



# Cahier des charges

## Fonctionnalités

**1:** Envoi de reçu au moment de la transaction (espèces ou chèque)  

**2:** Cochage facile du nom de l’usager lors de la réception du panier  

**3:** Envoi facile du nombre de commandes à la ferme  

**4:** Forte limitation de la possibilité de modifier une commande après l’envoi du reçu  

**5:** Voir l’argent qu’on est censé avoir sur Lydia et dans la caisse  

**6:** Voir des statistiques globales de l’année, du dernier mois, de la dernière semaine, etc  

**7:** Modifier le calendrier (dates futures seulement), notamment en cas d’annulation ou d’offre spéciale  

**8:** Affichage automatique de la liste des légumes dès la réception de l’information donnée par la ferme  

**9:** Post facebook automatique de la liste des légumes dès la réception de l’information donnée par la ferme  

**10:** Mails de rappels automatiques pour les usagers ayant manqué la première distribution du lundi, affichage GUI "les mails de rappel seront envoyé automatiquement à [20]h" avec un bouton pour annuler, puis "mails de rappel envoyés" après l'envoi.  


### Nice to have:

**NTH 1:** Suggestion automatique et autocomplétion des prénoms, noms, et adresses mail des gens basé sur notre db d’usagers et sur une db de l’ENSEA, pour avoir la pérénnité de cette fonctionnalité chaque nouvelle année  

**NTH 2:** Possibilité de modifier le modèle des prix, les type d’abonnements avec un GUI  

**NTH 3:** Dark mode  

**NTH 4:** Version anglaise  

**NTH 5:** Formulaire et terminal de paiment publique pour prendre les commandes des usagers (long-terme)  

**NTH 6:** Planning stands vente et distribution, et permanences BDLS.p

## Implémentation

**1:**  
.Pour chaque ligne du tableau, bouton ‘Envoyer reçu’  

**2:**  
.Faire disparaître les noms cochés de la base (avec une option pour les ré-afficher en cas d’erreur)  

**3:**  
.Bouton ‘envoyer commande à la ferme’  
.Si il y a un problème (donnée manquante, etc) message d’erreur, sinon, affichage d’un récapitulatif dans une fenêtre de confirmation, et envoi d’un mail à partir d’un template à la ferme  

**NTH 1:**  
.Accéder à la db de l'ensea en envoyant le même type de requête que l'autocomplétion dans le webmail ensea (webmail.ensea.fr/service/soap/AutoCompleteRequest)  
.Pour cela, d'abord établir une session authentifiée avec les compte ENSEA des Paniers Bio  

---
# Concept UI

<img src="./readme_imgs/Commandes3.png" width=800 />

---

<img src="./readme_imgs/Distribution2.png" width=800 />

---

![](./readme_imgs/UX_1.gif)

---  

# Architecture
<img src="./readme_imgs/Arch1.png" width="400"/>
<img src="./readme_imgs/Arch2.png" width="400"/>

# Ressouces éducatives

## Git
Traversy: https://www.youtube.com/watch?v=SWYqp7iY_Tc

## Généralités Web Development
Traversy Web Dev in 2019 guide: https://www.youtube.com/watch?v=UnTQVlqmDQ0  
https://www.w3schools.com/  
Traversy HTTP: https://www.youtube.com/watch?v=iYM2zFP3Zn0

Très bonne chaîne en français:
https://www.youtube.com/user/grafikarttv/playlists

## HTML
Traversy: https://www.youtube.com/watch?v=UB1O30fR-EE  

## CSS
Traversy: https://www.youtube.com/watch?v=yfoY53QXEnI

## JS
Traversy: https://www.youtube.com/watch?v=hdI2bqOjy3c

### NodeJS
https://www.w3schools.com/nodejs/nodejs_intro.asp  
Traversy: https://www.youtube.com/watch?v=fBNz5xF-Kx4  

### ExpressJS
Traversy: https://www.youtube.com/watch?v=L72fhGm1tfE  

### VueJS
Traversy: https://youtu.be/Wy9q22isx3U

## EXACTEMENT CE QU'ON VEUT: VENM Stack
Traversy: https://www.youtube.com/playlist?list=PLillGF-RfqbYSx-Ab1xWVanGKtowTsnNm

## MERN Stack (peut-être utile même si on n'utilise pas React)
Traversy: https://www.youtube.com/playlist?list=PLillGF-RfqbbiTGgA77tGO426V3hRF9iE  
