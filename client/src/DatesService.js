import axios from 'axios'

const url='http://localhost:5000/api/dates/'

class DatesService {
  // Get Dates
  static getDates() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url)
        const data = res.data
        resolve(data)
      } catch (err) {
        reject(err)
      }
    })
  }
  // Create Order
  static insertOrder(data) {
    return axios.post(url, {
      data
    })
  }
  // Delete Order(s)
  static deleteOrder(id) {
    return axios.delete(`${url}${id}`)
  }
}

export default DatesService
