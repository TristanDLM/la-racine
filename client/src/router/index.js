import Vue from 'vue'
import VueRouter from 'vue-router'
import Commandes from '../views/Commandes.vue'
import Distribution from '../views/Distribution.vue'
import Usagers from '../views/Usagers.vue'
import Stats from '../views/Stats.vue'
import Finances from '../views/Finances.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/distribution', name: 'Distribution', component: Distribution },
  { path: '/commandes', name: 'Commandes', component: Commandes },
  { path: '/stats', name: 'Stats', component: Stats },
  { path: '/finances', name: 'Finances', component: Finances },
  { path: '/usagers', name: 'Usagers', component: Usagers }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
