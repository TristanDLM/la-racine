import axios from 'axios'

const url='http://localhost:5000/api/users/'

class UsersService {
  // Get Users
  static getUsers() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url)
        const data = res.data
        resolve(data)
      } catch (err) {
        reject(err)
      }
    })
  }
  // Find last names from first name
  static findLastNamesByFirstName(firstName) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(`http://localhost:5000/api/users/lnbfn/?firstName=${firstName}`)
        const data = res.data
        resolve(data)
      } catch (err) {
        reject(err)
      }
    })
  }
  // Find user from first and last name
  static findUserByFirstLastName(firstName,lastName) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(`http://localhost:5000/api/users/fln/?firstName=${firstName}&lastName=${lastName}`)
        const data = res.data
        resolve(data)
      } catch (err) {
        reject(err)
      }
    })
  }
  // Create User
  static createUser(firstName,lastName,email) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.post(url ,{
          firstName: firstName,
          lastName: lastName,
          email: email
        })
        const data = res.data
        resolve(data)
      } catch (err) {
        reject(err)
      }
    })
  }
  // Delete User(s)
}

export default UsersService
