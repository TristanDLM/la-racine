const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const users = require('./routes/api/users.js')
const orders = require('./routes/api/orders.js')
const dates = require('./routes/api/dates.js')

const app = express();

// Bodyparser Middleware
app.use(bodyParser.json())

// DB config
const db = require('./config/keys').mongoURI

// Connect to mongo
mongoose
  .connect(db)
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err))

// Use routes
app.use('/api/users', users)
app.use('/api/orders', orders)
app.use('/api/dates', dates)

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server started on port ${port}`))
