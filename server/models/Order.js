const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Get ObjectId type
const ObjectId = Schema.Types.ObjectId;

// Create Schema
const OrderSchema = new Schema({
  userID: {
    type: ObjectId,
    ref: "user",
    required: true
  },
  type: {
    type: String,
    default: "U",
    required: true
  },
  startDate: {
    type: ObjectId,
    ref: "date",
    required: true
  },
  payMethod: {
    type: String,
    required: true
  },
  paymentDate: {
    type: Date,
    default: Date.now
  },
  paid: {
    type: Boolean,
    required: true
  },
  receiptSent: {
    type: Boolean,
    required: true
  },
  notes: {
    type: String,
    default: "",
    required: false
  },
  baskets: {
    type: Object,
    default: {
      1: {pickedUp: false, deposit: false}
    },
    required: true
  }
});

module.exports = Order = mongoose.model('order', OrderSchema);
