const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Get ObjectId type
const ObjectId = Schema.Types.ObjectId;

// Create Delivery Date Schema
const DelDateSchema = new Schema({
  date: {
    type: Date,
    required: true
  },
  weekNumber: {
    type: Number,
    required: true
  },
  types: {
    type: [{ 4: Number, 16: Number, U: Number}],
    required: true
  }
});

module.exports = DelDate = mongoose.model('date', DelDateSchema);
