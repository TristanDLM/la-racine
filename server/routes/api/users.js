const express = require('express')
const router = express.Router()

//User model
const User = require('../../models/User')

// @route   GET api/users
// @desc    Get All User
// @access  Public
router.get('/', (req,res) => {
  console.log('GET api/users')
  User.find()
    .then(users => res.json(users))
});

// @route   GET api/users/lnbfn
// @desc    Get User Last Names from First Name
// @access  Public
router.get('/lnbfn/', (req,res) => {
  console.log('GET api/users/lnbfn')
  User.find({ 'firstName': req.query.firstName })
    .then(result => res.json(result))
});

// @route   GET api/users/fln
// @desc    Get User from First and Last Name
// @access  Public
router.get('/fln/', (req,res) => {
  console.log('GET api/users/fln')
  User.find({ 'firstName': req.query.firstName, 'lastName': req.query.lastName })
    .then(result => res.json(result))
    .catch(err => res.status(404).json({success: false}))
});

// @route   POST api/users
// @desc    Create A User
// @access  Public
router.post('/', (req,res) => {
  console.log('POST api/users')
  const newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  })
  newUser.save().then(user => res.json(user))
})

// @route   DELETE api/users/:id
// @desc    Delete A User
// @access  Public
router
  .delete('/:id', (req,res) => {
    console.log('DELETE api/users/:id')
    User.findById(req.params.id)
    .then(user => user.remove().then(() => res.json({sucess: true})))
    .catch(err => res.status(404).json({success: false}))
})

module.exports = router
