const express = require('express');
const router = express.Router();

//DelDate model
const Date = require('../../models/DelDate');

// @route   GET api/dates
// @desc    Get All Dates
// @access  Public
router.get('/', (req,res) => {
  console.log('GET api/dates')
  Date.find()
    .then(dates => res.json(dates));
});

// @route   POST api/dates
// @desc    Create A Date
// @access  Public
router.post('/', (req,res) => {
  console.log('POST api/dates')
  const newDate = new Date({
    dates: req.body.dates,
    weekNumber: req.body.weekNumber,
    types: req.body.types
  });
  newDate.save().then(date => res.json(date));
});

// @route   DELETE api/dates/:id
// @desc    Delete A Date
// @access  Public
router.delete('/:id', (req,res) => {
  console.log('DELETE api/dates/:id')
  Date.findById(req.params.id)
  .then(date => date.remove().then(() => res.json({sucess: true})))
  .catch(err => res.status(404).json({success: false}));
});

module.exports = router;
