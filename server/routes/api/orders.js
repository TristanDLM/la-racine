const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

//Models
const Order = require('../../models/Order');
const User = require('../../models/User');
const DelDate = require('../../models/DelDate');

// Get ObjectId type
const ObjectId = mongoose.Types.ObjectId;

// @route   GET api/orders
// @desc    Get All Orders
// @access  Public
router.get('/', (req,res) => {
  console.log('GET api/orders')
  Order.find()
    .populate({path: 'userID', model: 'user'})
    .populate({path: 'startDate', model: 'date'})
    //.sort({paymentDate: -1})
    .then(orders => res.json(orders))
    .catch(err => console.log(err));
});

// @route   POST api/orders
// @desc    Create An Order
// @access  Public
router.post('/', (req,res) => {
  console.log('POST api/orders')
  const newOrder = new Order({
  	userID: ObjectId(req.body.userID),
  	type: req.body.type,
  	startDate: ObjectId(req.body.startDate),
    paymentDate: Date(),
  	payMethod: req.body.payMethod,
  	paid: req.body.paid,
  	receiptSent: req.body.receiptSent,
  	notes: req.body.notes,
    baskets: req.body.baskets
  });
  newOrder.save().then(order => res.json(order));
});

// @route   PUT api/orders
// @desc    Update An Order
// @access  Public
router.put('/', (req,res) => {
  console.log('PUT api/orders')
  // Validate request
  if(!req.body._id) {
        return res.status(400).send({
            message: "Order id can not be empty"
        })
    }
  Order.findByIdAndUpdate({"_id": req.body._id}, req.body, {new: true})
      .then(order => {
        if(!order) {
          return res.status(404).send({
            message: "Order not found with id " + req.body._id
          })
        }
        res.send(order)
      }).catch(err => {
        if(err.kind === 'ObjectId') {
          return res.status(404).send({
            message: "Order not found with id " + req.body._id
          })
        }
      return res.status(500).send({
        message: "Error updating order with id " + req.body._id
      })
    })
})

module.exports = router;
